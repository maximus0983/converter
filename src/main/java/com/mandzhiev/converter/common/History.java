package com.mandzhiev.converter.common;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@Entity
@Table
@XmlRootElement
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int accountId;
    private String valutein;
    private String valuteOut;
    private double summIn;
    private double summOut;
    private String date;

    public History() {
    }

    public History(int accountId, String valuteIn, String valuteOut, double summIn, double summOut, String date) {
        this.accountId = accountId;
        this.valutein = valuteIn;
        this.valuteOut = valuteOut;
        this.summIn = summIn;
        this.summOut = summOut;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getValutein() {
        return valutein;
    }

    public void setValutein(String valutein) {
        this.valutein = valutein;
    }

    public String getValuteOut() {
        return valuteOut;
    }

    public void setValuteOut(String valuteOut) {
        this.valuteOut = valuteOut;
    }

    public double getSummIn() {
        return summIn;
    }

    public void setSummIn(double summIn) {
        this.summIn = summIn;
    }

    public double getSummOut() {
        return summOut;
    }

    public void setSummOut(double summOut) {
        this.summOut = summOut;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        History history = (History) o;
        return id == history.id &&
                accountId == history.accountId &&
                Double.compare(history.summIn, summIn) == 0 &&
                Double.compare(history.summOut, summOut) == 0 &&
                Objects.equals(valutein, history.valutein) &&
                Objects.equals(valuteOut, history.valuteOut) &&
                Objects.equals(date, history.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountId, valutein, valuteOut, summIn, summOut, date);
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", valutein='" + valutein + '\'' +
                ", valuteOut='" + valuteOut + '\'' +
                ", summIn=" + summIn +
                ", summOut=" + summOut +
                ", date='" + date + '\'' +
                '}';
    }
}
