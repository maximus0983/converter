package com.mandzhiev.converter.common;

public enum Role {
    ADMIN,
    USER,
    ANONYMOUS;

    Role() {
    }
}
