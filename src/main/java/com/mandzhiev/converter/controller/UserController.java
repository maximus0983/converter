package com.mandzhiev.converter.controller;

import com.mandzhiev.converter.common.*;
import com.mandzhiev.converter.service.HistoryRepository;
import com.mandzhiev.converter.service.UserRepository;
import com.mandzhiev.converter.service.ValuteRepository;
import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private HistoryRepository historyRepository;
    @Autowired
    private ValuteRepository valuteRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RestTemplate restTemplate;


    @PostConstruct
    private void getAndWriteValute() {
        getCurs();
    }

    @GetMapping("/search/{valIn}/{valOut}/{date}")
    @ResponseBody
    public List<History> getHistory(@PathVariable String valIn, @PathVariable String valOut,
                                    @PathVariable String date) {
        int accountId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            accountId = ((CustomUserDetails) principal).getUser().getId();
        } else {
            accountId = -1;
        }
        return historyRepository.findByAccountIdAndDateAndValuteinAndValuteOut(accountId, date, valIn, valOut);
    }

    private ValCurs getCurs() {
        ValCurs valCurs = restTemplate.getForObject("http://www.cbr.ru/scripts/XML_daily.asp", ValCurs.class);
        assert valCurs != null;
        List<Valute> valuteList = valCurs.getValutes();
        for (int i = 0; i < valuteList.size(); i++) {
            valuteList.get(i).setDate(valCurs.getDate());
            valuteRepository.save(valuteList.get(i));
        }
        Valute valute = new Valute(1, "RUB", 1, "Российский рубль", "1");
        valute.setId("1");
        valute.setDate(valCurs.getDate());
        valuteRepository.save(valute);
        return valCurs;
    }

    //[Сумма в валюте конвертации(1)]=[Сумма в валюте конвертации(2)]*[Кратность(2)]*[Курс(1)]/[Кратность(1)]*[Курс(2)]
    @RequestMapping("/convert")
    @ResponseBody
    public double convert(@RequestParam("valute1") String valute1, @RequestParam("valute2") String valute2,
                          @RequestParam("summary") double summary) {
        Valute val1;
        Valute val2;
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd.MM.YYYY");
        String textDate = date.format(formatters);
        val1 = valuteRepository.findByCharCodeAndDate(valute1.substring(0, 3), textDate);
        if (val1 != null) {
            val2 = valuteRepository.findByCharCodeAndDate(valute2.substring(0, 3), textDate);
        } else {
            ValCurs valCurs = getCurs();
            val1 = valuteRepository.findByCharCodeAndDate(valute1.substring(0, 3), valCurs.getDate());
            val2 = valuteRepository.findByCharCodeAndDate(valute2.substring(0, 3), valCurs.getDate());
        }
        int multiplicity1 = val1.getNominal();
        double rate1 = Double.parseDouble(val1.getValue().replace(",", "."));
        int multiplicity2 = val2.getNominal();
        double rate2 = Double.parseDouble(val2.getValue().replace(",", "."));
        double result = summary * multiplicity2 * rate1 / (multiplicity1 * rate2);
        int accountId;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof CustomUserDetails) {
            accountId = ((CustomUserDetails) principal).getUser().getId();
        } else {
            accountId = -1;
        }
        historyRepository.save(new History(accountId, val1.getCharCode(), val2.getCharCode(), summary,
                Precision.round(result, 2), textDate));
        return Precision.round(result, 2);
    }

    @RequestMapping("/")
    public String showLoginPage() {
        return "homepage";
    }

    @GetMapping("/registration")
    public String showRegistrationForm() {
        return "registration";
    }

    @GetMapping("/docById")
    public String showDoc() {
        return "docById";
    }

    @GetMapping("/login")
    public String viewHomePage() {
        return "loginpage";
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "loginpage";
    }

    @PostMapping("/regController")
    public String registerUserAccount(@ModelAttribute Account account,
                                      BindingResult result) {

        Account existing = userRepository.findByUsername(account.getUsername());
        if (existing != null) {
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        if (result.hasErrors()) {
            return "registration";
        }
        account.setRole(Role.USER);
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        userRepository.save(account);
        UserDetails accountForSecurity = new CustomUserDetails(account);
        Authentication auth = new UsernamePasswordAuthenticationToken(accountForSecurity, null,
                accountForSecurity.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
        return "redirect:/";
    }
}