package com.mandzhiev.converter.service;

import com.mandzhiev.converter.common.Account;
import com.mandzhiev.converter.common.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    private final UserRepository userRepository;

    @Autowired
    public AccountService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDetails getByUsername(String username) {
        Account account = userRepository.findByUsername(username);
        return new CustomUserDetails(account);
    }
}
