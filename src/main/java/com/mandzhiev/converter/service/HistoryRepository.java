package com.mandzhiev.converter.service;

import com.mandzhiev.converter.common.History;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoryRepository extends JpaRepository<History, Long> {
    List<History> findByAccountIdAndDateAndValuteinAndValuteOut(int accountId, String date, String valutein, String valuteOut);
}
