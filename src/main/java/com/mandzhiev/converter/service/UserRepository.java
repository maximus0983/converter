package com.mandzhiev.converter.service;


import com.mandzhiev.converter.common.Account;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<Account, Long> {
    Account findByUsername(String username);
}
