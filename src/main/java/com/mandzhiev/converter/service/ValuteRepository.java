package com.mandzhiev.converter.service;

import com.mandzhiev.converter.common.Valute;
import org.springframework.data.repository.CrudRepository;

public interface ValuteRepository extends CrudRepository<Valute, Long> {
    Valute findByCharCodeAndDate(String CharCode, String date);
}
